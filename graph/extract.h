#ifndef __EXTRACT_H__
#define __EXTRACT_H__

#include <fstream>
#include <string>
#include <unordered_map>
#include "segment.h"
#include "connection.h"

class Extract {
    public:
    std::unordered_map<int, SegmentContainer*> connected_segs;
    std::unordered_map<int, SegmentConnectionContainer*> seg_connections;

    Extract(SegmentContainer* all_segs, SegmentConnectionContainer* all_connections);
    ~Extract();
    void extract_seg_and_conn(const std::unordered_map<int, std::set<int>>&);
    private:
    SegmentContainer* all_segs;
    SegmentConnectionContainer* all_connections;
    void extract_from_set(const std::set<int>&, SegmentContainer*, SegmentConnectionContainer*);
};
#endif // __EXTRACT_H__