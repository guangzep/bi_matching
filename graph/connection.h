#ifndef __CONNECTION_H__
#define __CONNECTION_H__

#include "graph_warpper.h"
#include "../utils/para.h"
//#include <c++/9/bits/c++config.h>
#include <unordered_map>
#include "map"


struct DirectedConnectionIdentity
{

};

struct ConnectionIdentity
{
    int start_contig_id;
    int end_contig_id;
    direction start_contig_direction;
    direction end_contig_direction;

    ConnectionIdentity(int start_contig_id, int end_contig_id, direction start_contig_dir, direction end_contig_dir);
    ConnectionIdentity(int start_contig_id, int end_contig_id);
    ~ConnectionIdentity() = default;
    bool operator==(const ConnectionIdentity &other) const
    {
        return (start_contig_id == other.start_contig_id
        && end_contig_id == other.end_contig_id
        && start_contig_direction == other.start_contig_direction
        && end_contig_direction == other.end_contig_direction);
    }

    bool operator >(const ConnectionIdentity &other) const
    {
        return (start_contig_id > other.start_contig_id || (start_contig_id == other.start_contig_id && end_contig_id > other.end_contig_id));
    }

    bool operator <(const ConnectionIdentity &other) const
    {
        return (start_contig_id < other.start_contig_id || (start_contig_id == other.start_contig_id && end_contig_id < other.end_contig_id));
    }
};


namespace std
{
    template<>
    struct hash<ConnectionIdentity>
    {
        std::size_t operator()(const ConnectionIdentity &k) const
        {
            using std::size_t;
            using std::hash;
            
            return ((hash<int>()(k.start_contig_id)
                        ^ (hash<int>()(k.end_contig_id) << 1)) >> 1)
                        ^ 
                    (hash<direction>()(k.start_contig_direction) << 1 );
        }
    };
}

class DirectedSegmentConnectionContainer
{

};
class SegmentConnectionContainer
{
    public:
    std::map<ConnectionIdentity, double> connections;

    public:
    SegmentConnectionContainer() = default;
    ~SegmentConnectionContainer() = default;
    int add_connection(int start, int end, double weight);
    inline bool contain_connection(int start, int end) const {
        ConnectionIdentity tmp(start, end);
//        ConnectionIdentity tmp2(end, start);
        bool r = this->connections.count(tmp) != 0;
//        bool r2 = this->connections.count(tmp2) != 0;
        return r ;
    };
    double get_weight(int start, int end);

};

#endif // __CONNECTION_H__