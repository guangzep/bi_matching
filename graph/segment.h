#ifndef __SEGMENT_H__
#define __SEGMENT_H__

#include <iostream>
#include <lemon/smart_graph.h>
#include <unordered_map>
#include <lemon/connectivity.h>
#include <set>
#include <string>

class Segment
{
    private:
        int id;
        bool complemented;
        int prev;
        int next;
        int head_node;
        int tail_node;
        std::vector<int> append_ids;
        std::string name;

    public:
        explicit Segment() = default;
        ~Segment() = default;
        Segment(int id, bool complemented, const std::string &name, int prev = -1, int next = -1, int head_node = -1, int tail_node = -1);
        Segment(const Segment &rhs);

        inline int get_id() const {return this->id;}
        inline std::vector<int> get_append_ids() const {return this->append_ids;}
        void set_append_id(int id);
        inline bool is_complemented() const {return  this->complemented;}
        inline int get_prev_node() const {return this->prev;}
        inline int get_next_node() const {return this->next;}
        inline int get_head_node() const {return this->head_node;}
        inline int get_tail_node() const {return this->tail_node;}
        
        inline void set_prev_node(int prev_node_id) {this->prev = prev_node_id;}
        inline void set_next_node(int next_node_id) {this->next = next_node_id;}
        inline void set_head_node(int head_node_id) {this->head_node = head_node_id;}
        inline void set_tail_node(int tail_node_id) {this->tail_node = tail_node_id;}
};


class SegmentContainer
{
    public:
        std::unordered_map<int, Segment *> segments;
        std::set<int> segment_ids;
    

    public:
        explicit SegmentContainer() = default;
        ~SegmentContainer();
        int insert_segments(int id, bool flipped, const std::string &name);
        int insert_segments(Segment*);
        inline bool contain_segment(int id) const  { return this->segments.count(id) != 0;}
};



#endif