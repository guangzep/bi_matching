#include "connection.h"

ConnectionIdentity::ConnectionIdentity(int start_contig_id, int end_contig_id)
: start_contig_id(start_contig_id), end_contig_id(end_contig_id), start_contig_direction(true), end_contig_direction(true)
{}

int SegmentConnectionContainer::add_connection(int start, int end, double weight) {
    if (this->contain_connection(start, end)) {
        return 1;
    }
    ConnectionIdentity conn(start, end);
    this->connections.emplace(conn, weight);
    return 0;
}

double SegmentConnectionContainer::get_weight(int start, int end) {
    ConnectionIdentity tmp(start, end);
    if (this->connections.count(tmp) == 0) return 0;
    return this->connections[tmp];
}