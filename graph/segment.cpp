#include "segment.h"

Segment::Segment(int id, bool flipped, const std::string &name, int prev, int next, int head_node, int tail_node)
: id(id), complemented(flipped), name(name), prev(prev), next(next), head_node(head_node), tail_node(tail_node)
{
    append_ids.push_back(id);
}

Segment::Segment(const Segment &rhs) 
{
    this->id = rhs.id;
    this->complemented = rhs.complemented;
    this->prev = rhs.prev;
    this->next = rhs.next;
    this->head_node = rhs.head_node;
    this->tail_node = rhs.tail_node;
    this->name = rhs.name;
    append_ids.push_back(rhs.id);
}
void Segment::set_append_id(int i) {
    this->append_ids.push_back(i);
}

SegmentContainer::~SegmentContainer()
{
    for (auto &i : segments)
    {
        delete i.second;
        i.second = nullptr;
    }

    segments.clear();
    segment_ids.clear();
}

int SegmentContainer::insert_segments(Segment* seg) {
    if(seg->get_id() == 144) {
        int i = 0;
    }
    this->segment_ids.insert(seg->get_id());
    this->segments.emplace(seg->get_id(), seg);
    return 1;
}

int SegmentContainer::insert_segments(int id, bool flipped, const std::string &name)
{
    if (this->contain_segment(id))
        return 1;
    this->segment_ids.insert(id);
    auto* seg = new Segment(id, flipped, name);
    this->segments.emplace(id, seg);
    return 0;
}