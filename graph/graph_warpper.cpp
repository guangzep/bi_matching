#include "graph_warpper.h"
#include "segment.h"
#include <cwchar>
#include <lemon/connectivity.h>
#include <lemon/core.h>
#include <lemon/list_graph.h>
#include <lemon/matching.h>
#include <lemon/smart_graph.h>



ConnectionGraph::ConnectionGraph(int nnodes, const std::set<int> &segment_id_set)
{
    graph.reserveNode(nnodes);
    int count = 0;
    segment_id = new lemon::SmartGraph::NodeMap<int>(this->graph);
    for (auto seg_id : segment_id_set)
    {
        auto tmp = this->graph.addNode();
        this->nodes.emplace(seg_id, tmp);
        (*this->segment_id)[tmp] = seg_id;
        count ++;
    }
    this->component_map = new lemon::SmartGraph::NodeMap<int>(this->graph);
    this->components_loaded = false;
}   

ConnectionGraph::~ConnectionGraph()
{
    delete this->segment_id;
    delete this->component_map;
    this->segment_id = nullptr;
    this->component_map = nullptr;
    this->graph.clear();
    this->nodes.clear();
}

void ConnectionGraph::add_edge(int u_seg, int v_seg) 
{
    const lemon::SmartGraph::Node &u_node = this->get_node(u_seg);
    const lemon::SmartGraph::Node &v_node = this->get_node(v_seg);
    this->graph.addEdge(u_node, v_node);
}

void ConnectionGraph::load_connected_components() 
{
    if (!this->components_loaded)
    {
        lemon::connectedComponents(this->graph, *(this->component_map));
        this->components_loaded = true;

        for (auto i : this->nodes)
        {
            int seg_id = i.first;
            auto node = i.second;
            int comp_id = (*this->component_map)[node];
            if (components.count(comp_id) == 0)
            {
                components[comp_id] = std::set<int>();
                components[comp_id].insert(seg_id);
            }
            else {components[comp_id].insert(seg_id);}
        }
    }
    else return;
}

//===============================================================================
//constructor

BpGraph::BpGraph(int n_seg, const std::set<int> &segment_id_set) 
{
    this->bp_graph.reserveNode(2*n_seg);
    int count = 0;
    head_id = new lemon::ListGraph::NodeMap<int>(this->bp_graph, -1);
    tail_id = new lemon::ListGraph::NodeMap<int>(this->bp_graph, -1);
    edge_weight = new lemon::ListGraph::EdgeMap<double>(this->bp_graph, 0);

    for (auto seg_id : segment_id_set)
    { 
        //head id even number
        //tail id odd number
        auto tmp = this->bp_graph.addNode();
        this->head_nodes.emplace(seg_id, tmp);
        auto tmpt = this->bp_graph.addNode();
        this->tail_nodes.emplace(seg_id, tmpt);
        (*this->head_id)[tmp] = seg_id;
        (*this->head_id)[tmpt] = -1;
        (*this->tail_id)[tmpt] = seg_id;
        (*this->tail_id)[tmp] = -1;

        count ++;
    }

    this->node_degree = new lemon::ListGraph::NodeMap<int>(this->bp_graph);
    
    this->starting_node_set = false;
    solver = nullptr;
    this->matching_seq_set = false;
}

BpGraph::~BpGraph() 
{
    delete this->head_id;
    delete this->tail_id;
    delete this->node_degree;
    delete this->solver;
    delete this->edge_weight; 
    this->head_id = nullptr;
    this->tail_id = nullptr;
    this->node_degree = nullptr;   
    
}

// TODO: change the two edge to different weight
// add edge, starting segment , end segment, 
void BpGraph::add_edge(int u_seg, int v_seg, double weight) 
{
    lemon::ListGraph::Node &u_head = this->head_nodes[u_seg];
    lemon::ListGraph::Node &u_tail = this->tail_nodes[u_seg];
    lemon::ListGraph::Node &v_head = this->head_nodes[v_seg];
    lemon::ListGraph::Node &v_tail = this->tail_nodes[v_seg];
    lemon::ListGraphBase::Edge e = lemon::findEdge(this->bp_graph, v_tail, u_head);
    if ( e != lemon::INVALID) {
        (*this->edge_weight)[e] = weight;
    } else {
        auto tmp = this->bp_graph.addEdge(u_tail, v_head);
        auto complement_tmp = this->bp_graph.addEdge(v_tail, u_head);
        this->edges.emplace(this->edges.size(), tmp);
        this->edges.emplace(this->edges.size(), complement_tmp);
        (*this->edge_weight)[tmp] = weight;
        (*this->edge_weight)[complement_tmp] = 0;
    }
}



void BpGraph::initialization() 
{
    if (this->solver == nullptr)
    {
        
        this->solver = new MaxWeightSolver(this->bp_graph, *this->edge_weight);
        for (auto e: this->edges) {
            std::cout<<(*this->edge_weight)[e.second]<<"\n";
        }
        solver->init();
        solver->run();
    }
    else return;
}

std::vector<std::vector<int>> BpGraph::get_matching_sequence() 
{
    if (this->matching_seq_set)
        return this->matching_seq;
    matching_seq_set = true;

    // run the max weight matching
    if (this->solver == nullptr)
        initialization();

    // now add pseudo edges between two nodes of contig to form connected comp
    lemon::ListGraph::EdgeMap<bool> edge_notin_matching(this->bp_graph, false);
    std::unordered_map<int, lemon::ListGraph::Edge> pseudo_edges;
    for (auto &i : head_nodes)
    {
        int seg_id = i.first;
        lemon::ListGraph::Node &s = head_nodes[seg_id];
        lemon::ListGraph::Node &e = tail_nodes[seg_id];

        lemon::ListGraph::Edge pseudo_edge = this->bp_graph.addEdge(s, e);
        (*this->edge_weight)[pseudo_edge] = 0;
        pseudo_edges[seg_id] = pseudo_edge;

        //enable the pseudo edge in subgraph filter
        edge_notin_matching[pseudo_edge] = true;
    }
    
    // now we create sub-graph adaptor that disable all edges not in the matching to form a matching graph.
    // and enable the pseudo edge 
    
    for (lemon::ListGraph::EdgeIt it(this->bp_graph); it != lemon::INVALID; ++it)
    {
        // keep edge if edge in the matching
        if (this->solver->matching(it))
            edge_notin_matching[it] = true;
    }   
    //now we get the matching graph with pseudo edge;
    lemon::FilterEdges<lemon::ListGraph> matching_graph(this->bp_graph, edge_notin_matching);
    
    // now apply the connected component algo.
    // each component is a match (either a sequence or a circle)
    lemon::FilterEdges<lemon::ListGraph>::NodeMap<int> comps(matching_graph);
    lemon::connectedComponents(matching_graph, comps);
    std::unordered_map<int, std::set<int>> connected_segs;
    
    for (auto &i: this->head_nodes)
    {
        int seg_id = i.first;
        auto h_node = i.second;
        auto t_node = this->tail_nodes[seg_id];
        int comp_id = comps[h_node];
        if(seg_id == 144){
            int mm = 1;
        }
        if (connected_segs.count(comp_id) == 0)
        {
            connected_segs[comp_id] = std::set<int>();
            connected_segs[comp_id].insert(seg_id);
        }
        else {
            connected_segs[comp_id].insert(seg_id);
        }
    }
    
    //now for each connected component
    for (auto &i: connected_segs)
    {
//        auto testn = this->tail_nodes[30];
//        auto mm = this->solver->mate(testn);
//        int seg_id = (*this->head_id)[mm];
        std::set<int> &seg_ids = i.second;
//        if(seg_ids.find(243) != seg_ids.end()){
//            int kll= 332;
//        }
        this->matching_seq.push_back(std::vector<int>());
        std::vector<int> &segment_sequence = this->matching_seq.back();
        // directly output matching 
        if (seg_ids.size() < 2)
        {
//            std::vector<lemon::ListGraph::Node> ends;
//            for (lemon::FilterEdges<lemon::ListGraph>::NodeIt it(matching_graph); it != lemon::INVALID; ++it)
//            {   auto t1 = (*this->head_id)[it];
//                auto t2 = (*this->tail_id)[it];
//                auto m1 = seg_ids.find(t1) != seg_ids.end();
//                auto m2 = seg_ids.find(t2) != seg_ids.end();
//                if (m1 || m2){
//                    int cnt = 0;
//                    for (lemon::FilterEdges<lemon::ListGraph>::IncEdgeIt e(matching_graph, it); e != lemon::INVALID; ++e)
//                    {
//                        cnt++;
//                    }
//                    if (cnt == 1)
//                        ends.push_back(it);
//                }
//            }
//            if (ends.size() == 0) {
//
//            }
            for (auto i: seg_ids)
                segment_sequence.push_back(i);
        }
        // now we try find the starting/end node; if degree == 1
        else 
        {
            std::vector<lemon::ListGraph::Node> ends;
            for (lemon::FilterEdges<lemon::ListGraph>::NodeIt it(matching_graph); it != lemon::INVALID; ++it)
            {   auto t1 = (*this->head_id)[it];
                auto t2 = (*this->tail_id)[it];
                auto m1 = seg_ids.find(t1) != seg_ids.end();
                auto m2 = seg_ids.find(t2) != seg_ids.end();
                if (m1 || m2){
                    int cnt = 0;
                    for (lemon::FilterEdges<lemon::ListGraph>::IncEdgeIt e(matching_graph, it); e != lemon::INVALID; ++e)
                    {
                        auto m = (*this->edge_weight)[e];
//                        if((*this->edge_weight)[e] != -1)
                            cnt++;
                    }
                    if (cnt == 1)
                        ends.push_back(it);
                }
            }
            
            if (ends.size() == 2) // line, output the sequence
            {
                lemon::ListGraph::Node start, end; //head tail
                if (this->bp_graph.id(ends[0]) % 2 == 0) //ends 0 is head
                {
                    end = ends[1];
                    start = ends[0];
                }
                else {
                    start = ends[1];
                    end = ends[0];
                }
                
                int starting_seg = (*this->head_id)[start];
                int end_seg = (*this->tail_id)[end];
                segment_sequence.push_back(starting_seg);
                auto mate = this->tail_nodes[starting_seg];
                int prev_seg_id = -1, j = 0;

                while (true)
                {
                    auto tmp = this->solver->mate(mate);
                    if (tmp == lemon::INVALID)
                        break;
                    int seg_id = (*this->head_id)[tmp];
                    if (seg_id == prev_seg_id)
                        break;
                    segment_sequence.push_back(seg_id);
                    if (seg_id == end_seg)
                        break;
                    mate = this->tail_nodes[seg_id];
                    if(j%2 !=0)
                        prev_seg_id = seg_id;
                    j++;
                }
            }
            
            else   // this means circle: find and delete the edge with minimal weight
            {
                std::cout<<"xxxxx"<<std::endl;
                std::set<int> nodes_id;
                for (auto id: seg_ids) {
                    if (id == 494) {
                        int mdd = 0;
                    }
                    nodes_id.insert(lemon::ListGraph::id(head_nodes[id]));
                    nodes_id.insert(lemon::ListGraph::id(tail_nodes[id]));
                }
                lemon::ListGraph::Edge minimal_edge;
                double min_w = 50000000000;
                lemon::ListGraph::Node n1, n2;
                for (lemon::FilterEdges<lemon::ListGraph>::EdgeIt eit(matching_graph); eit != lemon::INVALID; ++eit)
                {
                    if ((*this->edge_weight)[eit] == 0)
                        continue;
                    if ((*this->edge_weight)[eit] <= min_w)
                    {
                        n1 = matching_graph.u(eit);
                        n2 = matching_graph.v(eit);
                        auto kmk = lemon::ListGraph::id(n1);
                        if (nodes_id.find(lemon::ListGraph::id(n1)) != nodes_id.end() && nodes_id.find(lemon::ListGraph::id(n2)) != nodes_id.end()) {
                            min_w = (*this->edge_weight)[eit];
                            minimal_edge = eit;
                        }
                    }
                }
                // now we have the minimal_edge 
                // get the two node of the edge 
                n1 = matching_graph.u(minimal_edge);
                n2 = matching_graph.v(minimal_edge);

                lemon::ListGraph::Node start, end; //head tail
                if (this->bp_graph.id(n1) % 2 == 0) //n1 is head
                {
                    start = n1;
                    end = n2;
                }
                else {
                    start = n2;
                    end = n1;
                }


                int starting_seg = (*this->head_id)[start];
                int end_seg = (*this->tail_id)[end];
                segment_sequence.push_back(starting_seg);
                auto mate = this->tail_nodes[starting_seg];

                int prev_seg_id = -1, j = 0;
                while (true)
                {
                    auto tmp = this->solver->mate(mate);
                    if (tmp == lemon::INVALID)
                        break;
                    int seg_id = (*this->head_id)[tmp];
                    if (seg_id == prev_seg_id)
                        break;
                    segment_sequence.push_back(seg_id);
                    if (seg_id == end_seg)
                        break;
                    mate = this->tail_nodes[seg_id];
                    if(j%2 !=0)
                        prev_seg_id = seg_id;
                    j++;
                }
                segment_sequence.push_back(segment_sequence[0]);
            }
        }
    }

    //now consider connect the matching

    return this->matching_seq;
}


