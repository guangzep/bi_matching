#ifndef __GRAPH_WARPPER_H__
#define __GRAPH_WARPPER_H__

#include <lemon/fractional_matching.h>
#include <lemon/list_graph.h>
#include <lemon/smart_graph.h>
#include <lemon/connectivity.h>
#include <lemon/matching.h>
#include <type_traits>
#include <unordered_map>
#include <set>
#include <vector>

typedef lemon::MaxWeightedMatching<lemon::ListGraph, lemon::ListGraph::EdgeMap<double>> MaxWeightSolver; 

//use this for ngs and tgs directed info 
class BpGraph_Directed
{

};

//use this for barcode undirected info
class BpGraph
{
    public:
    lemon::ListGraph bp_graph;              // this is the origin bipartite graph

    // ==========================this is the structure storing node and edge info for original graph;
    lemon::ListGraph::NodeMap<int> *head_id;
    lemon::ListGraph::NodeMap<int> *tail_id;
    lemon::ListGraph::NodeMap<int> *node_degree;
    lemon::ListGraph::EdgeMap<double> *edge_weight;


    std::unordered_map<int, lemon::ListGraph::Edge> pseudo_edges;
    std::unordered_map<int, lemon::ListGraph::Edge> edges;
    std::unordered_map<int, lemon::ListGraph::Node> head_nodes;
    std::unordered_map<int, lemon::ListGraph::Node> tail_nodes;
    // =====================================================================================

    bool starting_node_set;
    bool circulization{};
    bool matching_seq_set; 
    lemon::ListGraph::Node starting_node; 
    MaxWeightSolver *solver;
    std::vector<std::vector<int>> matching_seq;

    // ========================private function===================================
    void format_matching_graph();



    public:
    BpGraph(int n_seg, const std::set<int> &segment_id_set);
    ~BpGraph();
    void add_edge(int u_seg, int v_seg, double weight);
    lemon::ListGraph::Node get_starting_node();
    //const MaxWeightSolver::MatchingMap & get_max_weight_matching();
    void initialization();  // call this func before query the result
    std::vector<std::vector<int>> get_matching_sequence();
};

// ================================================================================//
class ConnectionGraph
{
    private:
    lemon::SmartGraph graph;
    lemon::SmartGraph::NodeMap<int> *segment_id;
    lemon::SmartGraph::NodeMap<int> *component_map;
    std::unordered_map<int, lemon::SmartGraph::Node> nodes;
    std::unordered_map<int, std::set<int>> components;
    bool components_loaded;

    public:
    ConnectionGraph(int nnodes, const std::set<int> &segment_id_set);
    ~ConnectionGraph();
    void add_edge(int u_seg, int v_seg);
    void load_connected_components();
    inline std::unordered_map<int, std::set<int>> get_connected_components() {return this->components;}

    private:
    inline const lemon::SmartGraph::Node &get_node(int id) { return this->nodes[id]; }
};

#endif // __GRAPH_WARPPER_H__