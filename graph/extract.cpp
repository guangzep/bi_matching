#include "extract.h"

Extract::Extract (SegmentContainer *all_segs, SegmentConnectionContainer *all_connections)
: all_segs(all_segs), all_connections(all_connections){}

Extract::~Extract() {
    all_segs = nullptr;
    all_connections = nullptr;
//    for (auto& it :  connected_segs) {
//        delete it.second;
//        it.second = nullptr;
//    }
//    for (auto& it :  seg_connections) {
//        delete it.second;
//        it.second = nullptr;
//    }
}
void Extract::extract_seg_and_conn(const std::unordered_map<int, std::set<int>>& id_sets) {
    SegmentContainer* segs;
    SegmentConnectionContainer* connections;
    for (auto const& it : id_sets) {
        segs = new SegmentContainer();
        connections  = new SegmentConnectionContainer();
        this->extract_from_set(it.second, segs, connections);
        this->connected_segs.emplace(it.first, segs);
        this->seg_connections.emplace(it.first, connections);
    }
}

void Extract::extract_from_set(const std::set<int>& ids, SegmentContainer * sub_segs, SegmentConnectionContainer * sub_connections) {
    for (auto const& it : ids) {
        sub_segs->insert_segments(this->all_segs->segments[it]);
    }
    for (auto const& it : this->all_connections->connections) {
        int start_id = it.first.start_contig_id;
        int end_id = it.first.end_contig_id;
        double weight = it.second;
        if (ids.count(start_id) != 0 && ids.count(end_id) != 0) {
            sub_connections->add_connection(start_id, end_id, weight);
        }
    }
}