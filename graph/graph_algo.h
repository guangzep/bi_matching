#ifndef __GRAPH_ALGO_H__
#define __GRAPH_ALGO_H__

#include "graph_warpper.h"
#include "segment.h"
#include "connection.h"
#include <lemon/core.h>

int write_connected_comp(std::unordered_map<int, std::set<int>> &comp);
class connection_algo
{
    private:
    ConnectionGraph *graph;

    public:
    connection_algo(SegmentContainer *segments, SegmentConnectionContainer *connections);
    inline std::unordered_map<int, std::set<int>> get_connected_component() const {
        return this->graph->get_connected_components();
    }
    ~connection_algo();
};

class matching_algo
{
    private:
    BpGraph *graph;
    std::vector<int> recheck_cycles;
    public:
    matching_algo(SegmentContainer *segments, SegmentConnectionContainer *connections);
    ~matching_algo();
    inline std::vector<std::vector<int>> get_matching_sequence() const {
        return this->graph->get_matching_sequence();
    }

//    void recheck(std::vector<int>& seqs) {
//        auto seq_size = seqs.size();
//        for(int i = seq_size - 1; i >=0; i--) {
//            auto node = this->graph->head_nodes[seqs[i]];
//        }
//    }
};

#endif // __GRAPH_ALGO_H__