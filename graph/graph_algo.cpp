#include "graph_algo.h"

#include <fstream>

connection_algo::connection_algo(SegmentContainer *segments, SegmentConnectionContainer *connections) 
{
    this->graph = new ConnectionGraph(segments->segment_ids.size(), segments->segment_ids);

    //add edge here
    for (auto const& conn : connections->connections) {
        this->graph->add_edge(conn.first.start_contig_id, conn.first.end_contig_id);
    }
    this->graph->load_connected_components();

}

connection_algo::~connection_algo() {
    delete this->graph;
}

matching_algo::~matching_algo() {
    delete this->graph;
}

matching_algo::matching_algo(SegmentContainer *segments, SegmentConnectionContainer *connections)
{
    this->graph = new BpGraph(segments->segment_ids.size(), segments->segment_ids);

    //add edge here
    for (auto const& conn : connections->connections) {
        this->graph->add_edge(conn.first.start_contig_id, conn.first.end_contig_id, conn.second);
    }
//    this->graph->get_max_weight_matching();
}


int write_connected_comp(std::unordered_map<int, std::set<int>> &comp) 
{
    std::ofstream ss("connected_comp.txt");
    for (auto &i : comp)
    {
        ss << "#Group " << i.first << "\n";
        for (auto &j : i.second)
        {
            ss << j << "\t";
        }
        ss << std::endl;
    }
    ss.close();
    return 0;
}




