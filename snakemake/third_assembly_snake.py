fastq_dir = config['fastq_dir']
third_fqs = ["1.fq","2.fq"]

rule third_assembly:
    input:
        expand(third_fqs)
    output:
        "third_assembly/dbg.ctg.lay.gz"
    shell:
        "mkdir third_assembly && {config[wtdbg2]} -x {config[third_type]} -g {config[genome_size]} -t {config[max_thread]} -fo {output}/dbg -i {input}"
rule third_fasta:
    input:
        "third_assembly/dbg.ctg.lay.gz"
    output:
        "third_assembly/dbg.raw.fa"
    shell:
        "{config[wtpoa-cns]} -t {config[max_thread]} -i {input} -fo {output}"
rule third_polish1:
    input:
        fa="third_assembly/dbg.raw.fa",
        fqs=third_fqs
    output:
        "third_assembly/dbg.bam"
    shell:
        "{config[minimap2]} -t {config[max_thread]} -ax map-pb -r2k {input}.fa {input}.fqs | {config[samtools]} sort -@{config[max_thread]} > {output}"
rule all:
    input:
        bam="third_assembly/dbg.bam",
        fa="third_assembly/dbg.raw.fa"
    output:
        "third_assembly/dbg.cns.fa"
    shell:
        "{config[samtools]} view -F0x900 {input}.bam | {config[wtdbg2]}/wtdbg2-cns -t {config[max_thread]} -d {input}.fa -i - -fo {output}" 