workdir:config["workdir"]
fastq_dir = config['fastq_dir']
subworkflow pre:
    snakefile:
        "./third_assembly_snake.py"
    configfile:
        "snakeconf.json"
    workdir:
        config["workdir"]
rule all:
    input:
        "matching2/m2_scaffolds.fasta"

rule reduce_barcode:
    input:
        fq1=fastq_dir+"/{sample}_read_1.fq.gz",
        fq2=fastq_dir+"/{sample}_read_2.fq.gz"
    output:
        "split_barcode_out/{sample}.1.fq.gz",
        "split_barcode_out/{sample}.2.fq.gz"
    shell:
        "{config[perl]} {config[script_path]}/split_barcode.pl {config[script_path]}/barcode_list.txt  {input.fq1} {input.fq2} {output}"

rule merge_fq1:
    input:
        expand("split_barcode_out/{sample}.1.fq.gz", sample=config["sample_prefix"]),
    output:
        "merged_fq/merged.1.fq.gz"
    shell: "makir merged_fq && cat {input} {output}"

rule merge_fq2:
    input:
        expand("split_barcode_out/{sample}.2.fq.gz", sample=config["sample_prefix"]),
    output:
        "merged_fq/merged.2.fq.gz"
    shell: "cat {input} {output}"

rule preA:
    input:
        fq1="merged_fq/merged.1.fq.gz",
        fq2="merged_fq/merged.2.fq.gz"
    output:
        "preAssembly/pre_scaffolds.fasta.gz"
    shell:
        "makir preAssembly && {config[workdir]}/script/pe_assembly.sh {config[workdir]}/bin {config[max_memory]} {config[max_thread]} {config[workdir]}/preAssembly {input}.fq1 {input}.fq2"
rule unzip_fa:
    input:
        "preAssembly/pre_scaffolds.fasta.gz"
    output:
        "preAssembly/pre_scaffolds.fasta"
    shell:
        "gunzip {input}"
rule bam_2to3:
    input:
        fa2="preAssembly/pre_scaffolds.fasta",
        fa3=pre("third_assembly/dbg.cns.fa")
    output:
        "bam3/bwa_2to3.bam"
    shell:
        "makir bam3 && {config[bwa]} mem -t {config[max_thread]} {input}.fa3 {input}.fa2 | {config[samtools]} view -bS - > {output}"
rule extract_links:
    input:
        bam="bam3/bwa_2to3.bam",
        fa=pre("third_assembly/dbg.cns.fa")
    output:
        "matching1/links.tsv"
    shell:
        "makir matching1 && python3 {config[script_path]}/link_generator.py {input}.bam {input}.fa {output}"
rule matching1:
    input:
        "matching1/links.tsv"
    output:
        "matching1/order.tsv"
    shell:
        "{config[bin_path]}/bi_solve {input} {output}"
rule orientation11:
    input:
        "matching1/links.tsv",
        "matching1/order.tsv",

    output:
        "matching1/lst.lst",
        "matching1/vcf_file"
        # "matching1/orientaion.tsv"
    shell:
        "python {config[script_path]}/g_lst.py {input} {output}"
rule orientation12:
    input:
        "matching1/vcf_file"
    output:
        "matching1/vcf_file.gz"
    shell:
        "bgzip {input} && {config[bcftools]} index -t {output}"
rule orientation13:
    input:
        vcf="matching1/vcf_file.gz",
        lst="matching1/lst.lst"
    output:
        "matching1/solved_vcf_file.gz"
    shell:
        "{config[Spechap]} -v {input}.vcf -f {input}.lst -o {output}"

rule orientation14:
    input:
        "matching1/solved_vcf_file.gz",
        "matching1/links.tsv"
    output:
        "matching1/orientaion.tsv"
    # "matching1/orientaion.tsv"
    shell:
        "gunzip {input} && python {config[script_path]}/vcf_to_orient.py {input} {output}"
rule concat1:
    input:
        order="matching1/order.tsv",
        orientation="matching1/orientaion.tsv",
        fa="preAssembly/pre_scaffolds.fasta"
    output:
        "matching1/m1_scaffolds.fasta"
    shell:
        "python {config[script_path]}/concat.py {input} {output}"
rule bam_fq2fa:
    input:
        fq1="merged_fq/merged.1.fq.gz",
        fq2="merged_fq/merged.2.fq.gz",
        fa="matching1/m1_scaffolds.fasta"
    output:
        "matching2/fq2fa.bam"
    shell:
        "makir matching2 && {config[bwa]} mem -t {config[max_thread]} {input}.fa {input}.fq1 {input}.fq2 | {config[samtools]} view -bS - > {output}"
rule barcode_overlap:
    input:
        "matching2/fq2fa.bam"
    output:
        "matching2/overlap.tsv"
    shell:
        "{config[bin_path]}/barcode_overlap {input} {output}"
rule matching2:
    input:
        "matching2/overlap.tsv"
    output:
        "matching2/order.tsv"
    shell:
        "{config[bin_path]}/bi_solve {input} {output}"
rule jaccard_distance:
    input:
        "matching2/fq2fa.bam"
    output:
        "matching2/links.tsv"
    shell:
        "{config[bin_path]}/jaccard {input} {output}"

rule orientation21:
    input:
        "matching2/links.tsv",
        "matching2/order.tsv",

    output:
        "matching2/lst.lst",
        "matching2/vcf_file"
        # "matching1/orientaion.tsv"
    shell:
        "python {config[script_path]}/g_lst.py {input} {output}"
rule orientation22:
    input:
        "matching2/vcf_file"
    output:
        "matching2/vcf_file.gz"
    shell:
        "bgzip {input} && {config[bcftools]} index -t {output}"
rule orientation23:
    input:
        vcf="matching2/vcf_file.gz",
        lst="matching2/lst.lst"
    output:
        "matching2/solved_vcf_file.gz"
    shell:
        "{config[Spechap]} -v {input}.vcf -f {input}.lst -o {output}"

rule orientation24:
    input:
        "matching2/solved_vcf_file.gz",
        "matching2/links.tsv"
    output:
        "matching2/orientaion.tsv"
    # "matching1/orientaion.tsv"
    shell:
        "gunzip {input} && python {config[script_path]}/vcf_to_orient.py {input} {output}"
rule concat2:
    input:
        order="matching2/order.tsv",
        orientation="matching2/orientaion.tsv",
        fa="matching1/m1_scaffolds.fasta"
    output:
        "matching2/m2_scaffolds.fasta"
    shell:
        "python {config[script_path]}/concat.py {input} {output}"
        
    
