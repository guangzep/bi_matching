//
// Created by caronkey on 18/2/2021.
//
#include "graph/connection.h"
#include "graph/graph_algo.h"
#include "utils/graph_io.h"
#include "graph/extract.h"
#include "graph/segment.h"
struct w_and_direction{
    double w;
    bool direction;
};

w_and_direction * find_w(SegmentConnectionContainer *connections, const std::vector<int>& ids, int id) {
    double f_w = -100000;
    int d;
    for (int j = 0; j <2 || j >= ids.size() - 2 && j < ids.size(); j++ ){
        auto w = connections->get_weight(ids[j], id);
        auto w2 = connections->get_weight(id, ids[j]);
        if(w != 0 && f_w < w) {
            d = j;
            f_w = w;
        }
        if(w2 != 0 && f_w < w2) {
            d = j;
            f_w = w2;
        }
    }

    w_and_direction* wad = new w_and_direction();
    if(f_w != -100000) {
        wad->w = f_w;
        if (d == 0 || d == 1) {
            wad->direction = true;
        }
    }
    return wad;
}

double find_w_m(SegmentConnectionContainer *connections, const std::vector<int>& ids, const std::vector<int>& ids2) {
    double f_w = -100000;
    for (int j = 0; j <2 || j >= ids.size() - 2 && j < ids.size(); j++ ) {
        auto w = find_w(connections, ids2, ids[j]);
        if (w == 0) continue;
        if(f_w < w->w) f_w = w->w;
    }
    if(f_w != -100000) {
        return f_w;
    }
    return 0;
}
SegmentConnectionContainer* cycle_match(SegmentContainer *segments, SegmentConnectionContainer *connections, const std::vector<std::vector<int>>& sequences) {
//    fixme sequences duplication
        auto head_ids = new std::set<int>();
        for (auto const& i : sequences) {
            if (head_ids->count(i[0]) != 0){
                continue;
            }
//        merge matched sequences to one seg
//  fixme merge seg with orientation
            if (i.size() == 1) continue;
            auto head_seg = segments->segments[i[0]];
            head_ids->emplace(i[0]);
            for(int j = 1; j < i.size(); j++) {
                auto seg = segments->segments[i[j]];
                if (seg->get_id() == head_seg->get_id()){
                    head_seg->set_append_id(seg->get_append_ids()[0]);
                    continue;
                }
                for (auto const &k: seg->get_append_ids()) {
                    head_seg->set_append_id(k);
                }
            }
//        remove merged
            auto s = head_seg->get_append_ids();
            for(int j = 1; j < s.size(); j++) {
                if (s[j] == head_seg->get_id()) continue;
                if (s[j] == 144){
                    auto tm = segments->segments[144];
                    auto mkk = 1;
                }
                segments->segments.erase(s[j]);
                segments->segment_ids.erase(s[j]);
            }
        }
//    update connections
        SegmentConnectionContainer *n_cons =  new SegmentConnectionContainer();
        for (auto const &it : segments->segments) {
            for (auto const &it2 : segments->segments) {
                if (it.first >= it2.first)continue;
                int it_size = it.second->get_append_ids().size();
                int it2_size = it2.second->get_append_ids().size();
                double w = 0;
                if (it_size == 1 && it2_size == 1) {
                    w = connections->get_weight(it.first, it2.first);
                    if (w == 0) w = connections->get_weight(it2.first, it.first);
                } else if(it_size != 1 && it2_size == 1) {
                    w = find_w(connections, it.second->get_append_ids(), it2.first)->w;
                } else if(it_size == 1 && it2_size != 1) {
                    w = find_w(connections, it2.second->get_append_ids(), it.first)->w;
                } else if (it_size != 1 && it2_size != 1) {
                    w = find_w_m(connections, it.second->get_append_ids(), it2.second->get_append_ids());
                }

                if (w != 0) {
                    n_cons->add_connection(it.first, it2.first, w);
                }
            }
        }
//        matching_algo* m = new matching_algo(segments, n_cons);
//    connection_algo c_algo(segments, n_cons);
//    auto connected_component = c_algo.get_connected_component();
//    auto s =  connected_component[0].size();
    return n_cons;
}
//recheck if cycle in line path
void recheck(SegmentConnectionContainer* cons, std::vector<int>& segs, std::unordered_map<int, std::string>& id_to_name, std::ofstream& out) {
    std::vector<std::pair<int, int>> results;
    auto seq_size = segs.size();
    for(int i = seq_size - 1; i >=1; i--) {
        auto id1 = segs[i];
        for (int j = i - 1; j >=0; j--) {
            auto id2 = segs[j];
            if (cons->contain_connection(id1, id2)) {
                results.emplace_back(j, i);
            }
        }
    }
    for (auto item : results) {
        for (int  i = item.first; i <= item.second - 1; i++) {
            out<<id_to_name[segs[i]]<<'\t';
        }
        out<<id_to_name[segs[item.second]]<<"\trecheck";
        out<<"\n";
    }
}

// argv1 graph file argv2 original fasta argv3 scafold
int main(int argc, char** argv) {
    auto fn = argv[1];
    std::ofstream outf(argv[2]);
    
    SegmentContainer *segment_container = new SegmentContainer();
    SegmentConnectionContainer *connection_container = new SegmentConnectionContainer();

    GraphReader graph_loader(fn, segment_container, connection_container);

    connection_algo c_algo(segment_container, connection_container);
    Extract extract(segment_container, connection_container);
    auto connected_component = c_algo.get_connected_component();
    
    //write_connected_comp(connected_component);

    extract.extract_seg_and_conn(connected_component);
    for (auto const& it : extract.connected_segs) {
        matching_algo* m = new matching_algo(it.second, extract.seg_connections[it.first]);
//        matching_algo m_algo(it.second, extract.seg_connections[it.first]);

        auto sequences = m->get_matching_sequence();
        SegmentConnectionContainer* segc = extract.seg_connections[it.first];
        segc = cycle_match(it.second, segc ,sequences);
        int pre_size = sequences.size();
        int i = 0;
        while(i < 0) {
            m = new matching_algo(it.second, segc);
            sequences = m->get_matching_sequence();
            if(sequences.size() == pre_size) {
                break;
            } else
                pre_size = sequences.size();
            i++;
        }


        for(auto const& segs : it.second->segments) {
            auto append_ids = segs.second->get_append_ids();
            if (append_ids.size() <= 1) continue;
            if (append_ids.front() != append_ids.back()) {
                recheck(connection_container, append_ids, graph_loader.id_to_seg_name, outf);
            } else {
                for(int k = 0; k < segs.second->get_append_ids().size() - 2; k++) {
                    auto id = segs.second->get_append_ids()[k];
                    outf << graph_loader.id_to_seg_name[id] << "\t";
                }
                auto id = segs.second->get_append_ids()[segs.second->get_append_ids().size() - 2];
                outf << graph_loader.id_to_seg_name[id];
                outf <<"\n" ;
            }
//                outf << "cycle" << "\t";
//                continue;
//            for (auto const &id : segs.second->get_append_ids()) {
//                    outf << graph_loader.id_to_seg_name[id] << "\t";
//            }
        }

//        for (auto const& i : sequences) {
//            //outf <<i<<"-";
//            for(auto const &j: i)
//            {
//                auto t = it.second->segments[j];
//                for (auto const &m : t->get_append_ids()) {
//                    outf << m << "\t";
//                }
//            }
//            outf << "\n";
//        }
    }

    delete segment_container;
    delete connection_container;
    outf.close();
}