//
// Created by caronkey on 18/2/2021.
//
//arg1 graph file arg2 segment info arg3

#include <unordered_map>
#include <string>
#include <vector>
#include "utils/util.h"
#include "utils/graph_io.h"
#include "graph/graph_algo.h"
#include "graph/extract.h"
std::unordered_map<std::string , std::string>* load_seg_addition(const char* fn, const std::string &delimiters = "\t") {
    auto* result = new std::unordered_map<std::string, std::string>();
    std::ifstream inf(fn);
    std::string line;
    std::vector<std::string> buffer;
    while (std::getline(inf, line)) {
        tokenize(line, buffer, delimiters);
        std::string seg = buffer[0];
//      b1 start b2 end b3 depth
        std::string addition = "UNKNOWN\t" + buffer[1] + "\t" + buffer[2] + "\t" + buffer[3];
        result->emplace(seg, addition);
        buffer.clear();
    }
    inf.close();
    return result;
}


int main(int argc, char** argv) {
    auto seg_addition = load_seg_addition(argv[2]);
    SegmentContainer *segment_container = new SegmentContainer();
    SegmentConnectionContainer *connection_container = new SegmentConnectionContainer();
    GraphReader graph_loader(argv[1],segment_container, connection_container);
    connection_algo c_algo(segment_container, connection_container);
    Extract extract(segment_container, connection_container);
    auto connected_component = c_algo.get_connected_component();
    extract.extract_seg_and_conn(connected_component);

    int n = 0;
    for (auto& it : extract.seg_connections) {
        std::string prefix = argv[3];
        prefix.append(std::to_string(n));
        GraphWriter outf(prefix.c_str());
        outf.write_graph(it.second, extract.connected_segs[it.first], graph_loader.id_to_seg_name, seg_addition);
        n++;
    }

    delete segment_container;
    delete connection_container;

}
