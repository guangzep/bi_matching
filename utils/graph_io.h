//
// Created by caronkey on 2/17/21.
//

#ifndef __EXTRACT_CONNECTION_H__
#define __EXTRACT_CONNECTION_H__

#include <fstream>
#include <string>
#include <unordered_map>
#include "../graph/segment.h"
#include "../graph/connection.h"

class GraphReader {
public:
    GraphReader(const char *fn, SegmentContainer *segs, SegmentConnectionContainer *seg_conns, const std::string &delimiters = "\t");

    ~GraphReader() ;

    SegmentContainer *segment_container;
    SegmentConnectionContainer *connection_container;
    std::unordered_map<std::string, int> seg_name_to_id;
    std::unordered_map<int, std::string> id_to_seg_name;
private:
    void load_from_file(const char *fn, const std::string &delimiters = "\t");
};

class GraphWriter {
public:
    GraphWriter(const char *prefix);
    ~GraphWriter() = default;
    void write_graph(SegmentConnectionContainer* connections,SegmentContainer* segs, std::unordered_map<int, std::string>& id_to_seg_name, std::unordered_map<std::string, std::string>* seg_addition, const std::string &delimiters = "\t");
private:
    const char* prefix;     //TODO: change this
};

#endif // __EXTRACT_CONNECTION_H__
