//
// Created by caronkey on 2/17/21.
//

#include "graph_io.h"
#include "util.h"

GraphReader::GraphReader(const char *fn, SegmentContainer *segs, SegmentConnectionContainer *seg_cons, const std::string &delimiters) {
    this->segment_container = segs;
    this->connection_container = seg_cons;
    load_from_file(fn, delimiters);
}

GraphReader::~GraphReader() {
//    delete segment_container;
    segment_container = nullptr;
//    delete connection_container;
    connection_container = nullptr;
    seg_name_to_id.clear();
    id_to_seg_name.clear();
}

void GraphReader::load_from_file(const char *fn, const std::string &delimiters) {
    std::ifstream inf(fn);
    std::string line;
    std::vector<std::string> buffer;
    int n = 0;
    while (std::getline(inf, line)) {
        tokenize(line, buffer, delimiters);
        std::string seg1 = buffer[0];
        std::string seg2 = buffer[1];
        double weight = std::stod(buffer[2]);
        // add segment
        if (this->seg_name_to_id.find(seg1) == this->seg_name_to_id.end()) {
            this->seg_name_to_id.emplace(seg1, n);
            this->id_to_seg_name.emplace(n, seg1);
            this->segment_container->insert_segments(n, true, seg1);
            n++;
        }
        if (this->seg_name_to_id.find(seg2) == this->seg_name_to_id.end()) {
            this->seg_name_to_id.emplace(seg2, n);
            this->id_to_seg_name.emplace(n, seg2);
            this->segment_container->insert_segments(n, true, seg2);
            n++;
        }
        // add connection
        int id1 = this->seg_name_to_id[seg1];
        int id2 = this->seg_name_to_id[seg2];
        if (!this->connection_container->contain_connection(id1, id2)) {
            this->connection_container->add_connection(id1, id2, weight);
        }
        buffer.clear();
    }
    inf.close();
}

GraphWriter::GraphWriter(const char *prefix) { this->prefix = prefix; }

void GraphWriter::write_graph(SegmentConnectionContainer *connections, SegmentContainer *segs,
                              std::unordered_map<int, std::string> &id_to_seg_name,
                              std::unordered_map<std::string, std::string> *seg_addition,
                              const std::string &delimiters) {
    std::string connection_file = this->prefix;
    std::string seg_file = this->prefix;
    connection_file.append(".connection");
    seg_file.append(".segs");
    std::ofstream outf_conn(connection_file);
    std::ofstream outf_seg(seg_file);
    for (auto const &it : connections->connections) {
        auto start = id_to_seg_name[it.first.start_contig_id];
        auto end = id_to_seg_name[it.first.end_contig_id];
        outf_conn << start << delimiters << end << delimiters << it.second << "\n";
    }
    for (auto const &it : segs->segment_ids) {
        auto seg = id_to_seg_name[it];
        if (seg_addition != nullptr) {
            outf_seg << seg << "\t" << (*seg_addition)[seg] << "\n";
        } else {
            outf_seg << seg << "\n";
        }
    }
    outf_conn.close();
}
