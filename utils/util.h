#ifndef __UTIL_H__
#define __UTIL_H__

#include <string>
#include <vector>


void tokenize(const std::string &str, std::vector<std::string> &tokens, const std::string &delimiters = "\t", bool trimEmpty = true);

#endif // __UTIL_H__