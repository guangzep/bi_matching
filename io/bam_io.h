#ifndef __BAM_IO_H__
#define __BAM_IO_H__
#include <string>
#include "../utils/para.h"
class BamReader
{
    private:
    
    void file_open(const std::string &filename); //with index


    public:
    BamReader(const std::string &filename);
    BamReader(const char *filename);
    ~BamReader();
    void seek_region(std::string &contig, uint start, uint end);
};

#endif // __BAM_IO_H__