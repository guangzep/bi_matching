#include "fasta_io.h"
#include "../utils/util.h"
#include <fstream>
#include <vector>


FastaReader::FastaReader(const std::string &filename) 
{
    open_file(filename);
}

FastaReader::FastaReader(const char *filename) 
{
    std::string tmp(filename);
    open_file(tmp);
}

FastaReader::~FastaReader() 
{
    inf.close();
    infai.close();
}


void FastaReader::open_file(const std::string & filename) 
{
    
    inf.open(filename);
    std::string fai_filename = filename + ".fai";
    infai.open(fai_filename);
    
    std::string line;
    std::vector<std::string> buffer;
    while (std::getline(this->infai, line))
    {
        tokenize(line, buffer);
        std::string &name = buffer[0];
        uint contig_len = static_cast<uint>(std::stoul(buffer[1]));
        std::ifstream::streampos pos = static_cast<std::ifstream::streampos> (std::stoull(buffer[2]));

        this->contig_offset[name] = pos;
        this->contig_length[name] = contig_len;
    }
}   


std::string FastaReader::get_contig_seq(const std::string &name) 
{
    if (this->contig_length.count(name) == 0)
        return "";
    std::string buffer;
    
    this->inf.seekg(this->contig_offset[name]);
    std::getline(this->inf, buffer);
    return buffer;
}