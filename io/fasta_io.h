#ifndef __FASTA_IO_H__
#define __FASTA_IO_H__

#include <fstream>
#include <string>
#include <unordered_map>
#include "../utils/para.h"
#include "../utils/util.h"

class FastaReader
{
    private:
    std::ifstream inf;
    std::ifstream infai;
    std::unordered_map<std::string, std::ifstream::streampos> contig_offset;
    std::unordered_map<std::string, uint> contig_length;

    void open_file(const std::string & filename);

    public:
    FastaReader(const std::string &filename);
    FastaReader(const char *filename);
    ~FastaReader();

    std::string get_contig_seq(const std::string &name);
    inline uint get_contig_length(const std::string &name) 
    {
        return this->contig_length[name];
    }

};


class FastaWriter
{

};


#endif // __FASTA.IO_H__