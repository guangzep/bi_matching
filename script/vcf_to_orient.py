import sys
vcf_file = sys.argv[1]
link_file = sys.argv[2]
orien_file = sys.argv[3]
def count(link_file,barcode=True):
	support_dict = {}
	gap_seq = {}
	for line in open(link_file):
		eles = line.strip().split("\t")
		seq1 = eles[0]
		seq2 = eles[1]
		o1 = eles[3]
		o2 = ""
		if eles[4] == "0":
			o2 = "1"
		else:
			o2 = "0"
		w = eles[2]
		rs = eles[5]
		s = 0
		if barcode:
			iw = int(w)
			if iw > 100:
				iw = 100+iw/100
			s = int(iw)
		else:
			s = int(2000/int(w))
		if seq1 in support_dict.keys():
			support_dict[seq1] = support_dict[seq1]+o1*s
		else:
			support_dict[seq1] = o1*s
		
		if seq2 in support_dict.keys():
			support_dict[seq2] = support_dict[seq2]+o2*s
		else:
			support_dict[seq2] = o2*s
		if rs != "N":
			gap_seq[seq1] = rs
		else:
			gap_seq[seq1] = "N"*100
	return support_dict, gap_seq

def parse_vcf(vcf_file, link_file, orien_file):
	orient = {}
	support_dict, gap_seq = count(link_file)
	f = open(orien_file, "w")
	for line in open(vcf_file):
		if line.startswith("chr1"):
			eles = line.strip().split("\t")
			if eles[2] in support_dict.keys():
				l = len(support_dict[eles[2]])
				if eles[9][1] == "/" and (support_dict[eles[2]].count("0") == l or support_dict[eles[2]].count("1") == l):
					orient[eles[2]] = support_dict[eles[2]][0]
					continue
			orient[eles[2]] = eles[9][0]
	
	for k,v in orient.items():
		if k in gap_seq.keys():
			f.write(k+"\t"+v+"\t"+gap_seq[k]+"\n")
		else:
			f.write(k+"\t"+v+"\t"+"N"*100+"\n")

parse_vcf(vcf_file, link_file, orien_file)