import sys
link_file = sys.argv[1]
order_file = sys.argv[2]

lst_file = sys.argv[3]
vcf_file = sys.argv[4]



def count(link_file,barcode=True):
	support_dict = {}
	gap_seq = {}
	for line in open(link_file):
		eles = line.strip().split("\t")
		seq1 = eles[0]
		seq2 = eles[1]
		o1 = eles[3]
		o2 = ""
		if eles[4] == "0":
			o2 = "1"
		else:
			o2 = "0"
		w = eles[2]
		rs = eles[5]
		s = 0
		if barcode:
			iw = int(w)
			if iw > 100:
				iw = 100+iw/100
			s = int(iw)
		else:
			s = int(2000/int(w))
		if seq1 in support_dict.keys():
			support_dict[seq1] = support_dict[seq1]+o1*s
		else:
			support_dict[seq1] = o1*s
		
		if seq2 in support_dict.keys():
			support_dict[seq2] = support_dict[seq2]+o2*s
		else:
			support_dict[seq2] = o2*s
		if rs != "N":
			gap_seq[seq1] = rs
		else:
			gap_seq[seq1] = "N"*100
	return support_dict, gap_seq

def mk_vcf(order_file,vcf_file,support_dict):
	vcf_fn = open(vcf_file, "w")
	vcf_fn.write("##fileformat=VCFv4.1\n")
	vcf_fn.write("##contig=<ID=chr1,length=249250621>\n")
	vcf_fn.write("##INFO=<ID=NS,Number=1,Type=Integer,Description=Number of Samples With Mapped Reads>\n")
	vcf_fn.write("##FORMAT=<ID=GT,Number=1,Type=String,Description=Genotype>\n")
	vcf_fn.write("#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	DEMO\n")
	pos={}
	i = 1
	for line in open(order_file):
		seqs = line.strip().split("\t")
		for s in seqs:
			pos[s] = i
			if s in support_dict.keys():
				if support_dict[s].count("0") == len(support_dict[s]):
					vcf_fn.write("chr1\t" + str(i) +	"\t" + s + "\tA" + "\tC" + "\t129" + "\t."+"\tNS=65"+"\tGT"+"\t0|1\n")
					i = i+1
					continue
				if support_dict[s].count("1") == len(support_dict[s]):
					vcf_fn.write("chr1\t" + str(i) +	"\t" + s + "\tA" + "\tC" + "\t129" + "\t."+"\tNS=65"+"\tGT"+"\t1|0\n")
					i = i+1
					continue
			vcf_fn.write("chr1\t" + str(i) +	"\t" + s + "\tA" + "\tC" + "\t129" + "\t."+"\tNS=65"+"\tGT"+"\t0/1\n")
			i = i+1
	return pos

def to_lst(link_file):
	support_dict, gap_seq = count(link_file)
	pos = mk_vcf(order_file, vcf_file, support_dict)
	lst_fn = open(lst_file, "w")
	i = 1
	for line in open(order_file):
		seqs = line.strip().split("\t")
		seqs_splits = [seqs[i:i + 1] for i in range(0, len(seqs), 1)]
		for ss in seqs_splits:
			lst_fn.write(str(len(ss)) + " ")
			lst_fn.write("uniq"+str(i)+" ")
			for s in ss:
				lst_fn.write(str(pos[s])+" ")
				if s in support_dict.keys():
					lst_fn.write(support_dict[s]+" ")
				else:
					lst_fn.write("0"*100+" ")
				lst_fn.write("82 40\n")
			i = i+1
to_lst(link_file)

	